#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code diffusé aux étudiants de BUT1 dans le cadre de la SAE 2.02: Exploration algorithmique d'un problème.

IUT d'Orleans BUT1 Informatique 2021-2022 
"""

import networkx as nx

# import pour la sauvegarde du graphe dans un fichier
import json
import matplotlib.pyplot as plt

def creation_graphe_avec_json():
    """Créer le graphe visuel d'une bande de données ouverte pendant la fonction

    Returns:
        Graph : un graphe de données
    """
    G = nx.Graph() # création du graphe vide
    plt.figure(figsize=(10,10))
    try:
        fichier = open("data/datatest.json")
        #fichier = open("C:\\PythonProject\\sae-maths-graphe\\data\\datatest.json", "r", encoding="utf-8")
        content = fichier.readlines()
        fichier.close()
    except:
        print("Le fichier n'existe pas !")
    for lignes in content :
        lignes = json.loads(lignes) # convertir la chaine str en dictionnaire
        #print(lignes["cast"])
        liste_acteurs_aretes = []
        acteur = 0
        ind = 0
        while ind < len(lignes['cast'])-1 and acteur < len(lignes['cast'])-1:
            if not G.has_edge(lignes['cast'][acteur], lignes['cast'][ind+1]) and (lignes['cast'][acteur]!=lignes['cast'][ind+1]):
                # regarde si il y a déjà une arête entre deux acteurs, permet d'optimiser le code
                liste_acteurs_aretes.append((lignes['cast'][acteur], lignes['cast'][ind+1]))
            ind += 1
            if ind == len(lignes['cast'])-1:
                acteur+=1
                ind = 0
        #print(liste_acteurs_aretes)
        G.add_edges_from(liste_acteurs_aretes)
    options = {
      'node_color' : 'orange',
      'edge_color' : 'pink',
      'node_size'  : 150,
      'font_size' : 10,
      'with_labels': True,
    }
    nx.draw(G, **options)
    plt.show()
    return G

graphe_json = creation_graphe_avec_json()

def collaborateur_commun(Graphe, acteur1, acteur2):
    ens_acteur_commun = set()
    for acteurs in Graphe.nodes():
        if (acteurs!=acteur1 and acteurs!=acteur2) and (acteurs in Graphe.adj[acteur1] and acteurs in Graphe.adj[acteur2]):
           ens_acteur_commun.add(acteurs)
    return ens_acteur_commun

collaborateurs_entre_deux_acteurs = collaborateur_commun(graphe_json, "Núria Espert", "Rosa Maria Sardà")
print(collaborateurs_entre_deux_acteurs)

def collaborateurs_proches(G,u,k):
    """Fonction renvoyant l'ensemble des acteurs à distance au plus k de l'acteur u dans le graphe G. La fonction renvoie None si u est absent du graphe.
    
    Parametres:
        G: le graphe
        u: le sommet de départ
        k: la distance depuis u
    """
    if u not in G.nodes: # 0(1)
        print(u,"est un illustre inconnu") 
        return None
    collaborateurs = set() # O(1)
    collaborateurs.add(u) # O(1)
    #print(collaborateurs)
    for i in range(k): # s'éxecute k fois
        collaborateurs_directs = set() # O(1)
        for c in collaborateurs: # s'éxcute n fois 
            for voisin in G.adj[c]: # s'éxecute n fois
                if voisin not in collaborateurs: # O(1)
                    collaborateurs_directs.add(voisin) # O(1)
        collaborateurs = collaborateurs.union(collaborateurs_directs) # O(1)
    return collaborateurs

print(collaborateurs_proches(graphe_json, "Núria Espert", 1))

def trouver_distance_k(G, acteur1, acteur2):
    """trouve la distance dans le graphe entre les deux acteurs passés en paramètres

    Args:
        G (graph): Graphe de données
        acteur1 (String): acteur faisant partis des sommets du graphe
        acteur2 (String): acteur faisant partis des sommets du graphe

    Returns:
        tuple : renvoie le tuple ( (acteur1, acteur2), distance entre ces deux acteurs ) 
    """
    collaborateurs = {acteur1}
    acteur_parcouru = set()
    distance_k = 0
    if acteur2 not in G.nodes:
        return str(acteur2) + " n'est pas dans le graphe"
    if acteur1 not in G.nodes:
        return str(acteur1) + " n'est pas dans le graphe"
    fini = False
    while not fini:
        collaborateurs_directs = set()
        for c in collaborateurs: # s'éxcute n fois
            if c in acteur_parcouru: #O(1)
                continue # optimise le code. 
            else:
                acteur_parcouru.add(c)
            if acteur2 in G.adj[c]:
                distance_k+=1
                return ({acteur1, acteur2}, distance_k)
            else:
                for voisin in G.adj[c]: # s'éxecute n fois
                    if voisin not in collaborateurs: # O(1)
                        collaborateurs_directs.add(voisin) # O(1)
                collaborateurs = collaborateurs.union(collaborateurs_directs) # O(1)
        distance_k+=1
    return None

print(trouver_distance_k(graphe_json, "Núria Espert", "Rosa Maria Sardà"))


def centralite_acteur(G, v):
    """Détermine la centralité d'un acteur qui est donné en paramètre

    Args:
        G : un graphe donné
        v : un acteur dans ce graphe

    Returns:
        tuple: le couple (acteur, distance) avec la distance de l'acteur par rapport à v
    """
    dico_centralite_acteur = {}
    for acteur in G.nodes:
        if acteur == v:
            dico_centralite_acteur[acteur] = 0
        else:
            chemin = trouver_distance_k(G, v, acteur)[1]

            #chemin = nx.shortest_path(G, v, acteur) 
            # # la bibilothèque inclut une fonction qui trouve le plus court chemin entre deux acteurs dans un graphe 
            # chemin contient une liste de sommets qui trace le chemin entre l'acteur source et l'acteur que l'on parcourt dans la boucle
            
            #dico_centralite_acteur[acteur] = len(chemin) - 1
            dico_centralite_acteur[acteur] = chemin

    # recherche de la valeur maximale dans le dictionnaire
    acteur_max_dist = None
    max_dist = 0
    for acteur, distance in dico_centralite_acteur.items():
        if distance > max_dist:
            max_dist = distance
            acteur_max_dist = acteur
    return acteur_max_dist, max_dist

print(centralite_acteur(graphe_json, "Victoria Abril"))


def centralite_du_graphe(G):
    """Renvoie la centralité d'un graphe, l'acteur central du graphe donné

    Args:
        G: un graphe donné

    Returns:
        dict: le couple { acteur : entier représentant une distance minimale dans toutes les plus grande distance entre chaque acteur qui ont un chemin }
    """
    liste_centralite_acteur = []
    for sommet in G.nodes:
        c = centralite_acteur(G, sommet)
        liste_centralite_acteur.append(c)

    # initialise la première valeur de la liste en tant que minimum
    minimum = dict() # récupère la première valeur de la liste, qui est un couple { clé : valeur }
    premier_elem = liste_centralite_acteur[0]
    minimum[premier_elem[0]] = premier_elem[1]

    # recherche de minimum
    for couple in liste_centralite_acteur:
        couple_actuel = {couple[0]: couple[1]}
        if list(couple_actuel.values())[0] < list(minimum.values())[0]:
            minimum = couple_actuel
    return minimum

print(centralite_du_graphe(graphe_json))


def distance_max_toute_paire_acteurs(G):
    """Fonction qui renvoie la distance maximale entre deux acteurs

    Args:
        G: un graphe donné

    Returns:
        dict: le couple { acteur1, acteur2 : distance entre eux }
    """
    max_distance = dict()
    # recherche de max pour toutes les distances de paire acteurs/acteurs dans le graphe
    for acteur in G.nodes:
        for a in G.nodes:
            chemin_entre_a_et_acteur = trouver_distance_k(G, acteur, a)[1]
            #chemin_entre_a_et_acteur = nx.shortest_path(G, acteur, a)
            if len(max_distance)==0 and a==acteur: 
            # on suppose que la seule possibilité que max_distance soit vide c'est qu'on commence à boucler, 
            # dans ce cas les deux premiers éléments a et acteur seront les mêmes puisqu'on parcourt la même liste pour les deux boucles
                max_distance[acteur + ", " + a] = 0
                varActeur = acteur
                varA = a

            elif chemin_entre_a_et_acteur > max_distance.get(varActeur + ", " + varA) :
            #elif len(chemin_entre_a_et_acteur)-1 > max_distance.get(varActeur + ", " + varA) :
                del max_distance[varActeur + ", " + varA]
                max_distance[acteur + ", " + a] = chemin_entre_a_et_acteur
                #max_distance[acteur + ", " + a] = len(chemin_entre_a_et_acteur)-1
                varActeur = acteur
                varA = a

    return max_distance

print(distance_max_toute_paire_acteurs(graphe_json))



# Partie 6, Bonus 

def centralite_groupe_acteur(groupe_acteur):
    G = nx.Graph()
    for acteur in groupe_acteur:
        print(acteur)
    return None
